package gitlabutil

import (
	"crypto/tls"
	"net/http"
	"strings"
	"time"

	"github.com/pkg/errors"
	gitlab "github.com/xanzy/go-gitlab"
)

func httpClientWithTimeout(timeout time.Duration, skipSSLVerify bool) *http.Client {
	tr := &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: skipSSLVerify,
		},
	}
	client := &http.Client{
		Timeout:   timeout,
		Transport: tr,
	}
	return client
}

// NewClient gitlab handle
func NewClient(token string, apiURL string, skipSSLVerify bool) (*gitlab.Client, error) {
	if len(token) == 0 {
		return nil, errors.New("Gitlab user token not set")
	}
	client := gitlab.NewClient(
		httpClientWithTimeout(time.Second*90, skipSSLVerify),
		token)
	if len(apiURL) > 0 {
		if !strings.HasSuffix(apiURL, "/") {
			apiURL += "/"
		}
		if !strings.HasSuffix(apiURL, "api/v4/") {
			apiURL += "api/v4/"
		}
		client.SetBaseURL(apiURL)
	}
	return client, nil
}
